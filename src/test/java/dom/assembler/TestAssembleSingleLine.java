package dom.assembler;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestAssembleSingleLine
{
	@Test
	public void testMOV()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("MOV");
		line.setFirstArg("r1");
		line.setSecondArg("r2");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000001101100010000100000000", program);
	}

	@Test
	public void testMOVI()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("movi");
		line.setFirstArg("r1");
		line.setSecondArg("00110010");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000001101101110000100110010", program);
	}

	@Test
	public void testNOP()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("nop");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000001101101110011000000000", program);
	}

	@Test
	public void testJUMP()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("jump");
		line.setFirstArg("R3");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000001001101100011011000000000", program);
	}

	@Test
	public void testJUMPI()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("JUMPI");
		line.setFirstArg("01010101");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000001001101101110011001010101", program);
	}

	@Test
	public void testJZ()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("JZ");
		line.setFirstArg("R5");
		line.setSecondArg("00001111");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000010001101011110011000001111", program);
	}

	@Test
	public void testJNZ()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("JNZ");
		line.setFirstArg("R4");
		line.setSecondArg("11110000");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000011001101001110011011110000", program);
	}

	@Test
	public void testADD()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("and");
		line.setFirstArg("R3");
		line.setSecondArg("R4");
		line.setThirdArg("r5");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000000101000101001100000000", program);
	}

	@Test
	public void testADDI()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("AdDi");
		line.setFirstArg("r2");
		line.setSecondArg("r4");
		line.setThirdArg("00100000");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000000001001110001000100000", program);
	}

	@Test
	public void testAND()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("and");
		line.setFirstArg("R3");
		line.setSecondArg("R4");
		line.setThirdArg("r5");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000000101000101001100000000", program);

	}

	@Test
	public void testANDI()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("ANDI");
		line.setFirstArg("R3");
		line.setSecondArg("R4");
		line.setThirdArg("00100000");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000000101001110001100100000", program);
	}

	@Test
	public void testLOAD()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("LOAD");
		line.setFirstArg("r5");
		line.setSecondArg("r0");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000001101100000110100000000", program);
	}

	@Test
	public void testLOADI()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("LOADI");
		line.setFirstArg("R5");
		line.setSecondArg("01000000");

		String program = MainClass.assembleSingleLine(line);

		assertEquals("00000000001101101110110101000000", program);
	}
}
