package dom.assembler;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class TestParseProgram
{
	@Test
	public void testExtractTokens()
	{
		String singleLine = "mov r1, r2";
		List<String> lines = new ArrayList<>();
		lines.add(singleLine);

		List<ProgramLine> programLines = MainClass.parseProgram(lines);

		assertEquals(1, programLines.size());

		ProgramLine processedLine = programLines.get(0);

		assertEquals("mov", processedLine.getCommand());
		assertEquals("r1", processedLine.getFirstArg());
		assertEquals("r2", processedLine.getSecondArg());
		assertNull(processedLine.getThirdArg());
	}

	@Test
	public void testProcessLineWithComment()
	{
		String singleLine = "nop ; some comment here";
		List<String> lines = new ArrayList<>();
		lines.add(singleLine);

		List<ProgramLine> programLines = MainClass.parseProgram(lines);

		assertEquals(1, programLines.size());

		ProgramLine processedLine = programLines.get(0);

		assertEquals("nop", processedLine.getCommand());
		assertNull(processedLine.getFirstArg());
		assertNull(processedLine.getSecondArg());
		assertNull(processedLine.getThirdArg());
	}

	@Test
	public void processLineWithCommentAndArgs()
	{
		String singleLine = "mov r1, r2; and some bullshit here: r2 mov, bla, r4, 0x32 to check if assembler works";
		List<String> lines = new ArrayList<>();
		lines.add(singleLine);

		List<ProgramLine> programLines = MainClass.parseProgram(lines);

		assertEquals(1, programLines.size());

		ProgramLine processedLine = programLines.get(0);

		assertEquals("mov", processedLine.getCommand());
		assertEquals("r1", processedLine.getFirstArg());
		assertEquals("r2", processedLine.getSecondArg());
		assertNull(processedLine.getThirdArg());
	}

	@Test
	public void testParseLineWithHexConstant()
	{
		String line = "movi r1, 0x10";
		List<String> program = new ArrayList<>();
		program.add(line);

		List<ProgramLine> lines = MainClass.parseProgram(program);

		assertEquals(1, lines.size());

		ProgramLine processedLine = lines.get(0);

		assertEquals("movi", processedLine.getCommand());
		assertEquals("r1", processedLine.getFirstArg());
		assertEquals("00010000", processedLine.getSecondArg());
	}

	@Test
	public void testParseLineWithBinaryConstant()
	{
		String line = "movi r1, 0b1";
		List<String> program = new ArrayList<>();
		program.add(line);

		List<ProgramLine> lines = MainClass.parseProgram(program);

		assertEquals(1, lines.size());

		ProgramLine processedLine = lines.get(0);

		assertEquals("movi", processedLine.getCommand());
		assertEquals("r1", processedLine.getFirstArg());
		assertEquals("00000001", processedLine.getSecondArg());
	}

	@Test
	public void testParseLineWithDecimalConstant()
	{
		String line = "movi r1, 0d255";
		List<String> program = new ArrayList<>();
		program.add(line);

		List<ProgramLine> lines = MainClass.parseProgram(program);

		assertEquals(1, lines.size());

		ProgramLine processedLine = lines.get(0);

		assertEquals("movi", processedLine.getCommand());
		assertEquals("r1", processedLine.getFirstArg());
		assertEquals("11111111", processedLine.getSecondArg());
	}
}
