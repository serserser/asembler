package dom.assembler;

import org.junit.Ignore;
import org.junit.Test;
import sun.applet.Main;

import static org.junit.Assert.*;

public class TestGetAssemblerLineInfo
{
	@Test
	public void testMOV_R1_R2_mixed()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("MOV");
		line.setFirstArg("r1");
		line.setSecondArg("r2");
		line.setThirdArg(null);

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value of alu op should be 11", "11", info.getAlu());
		assertEquals("The value for write register should be r1 001", "001", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register 110", "110", info.getArg1());
		assertEquals("The value for second arg should be r2 010", "010", info.getArg2());
		assertEquals("This should not be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This should not be an memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 0000000 (default)", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter value should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testMOV_R1_R2_upper()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("MOV");
		line.setFirstArg("R1");
		line.setSecondArg("R2");
		line.setThirdArg(null);

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value of alu op should be 11", "11", info.getAlu());
		assertEquals("The value for write register should be r1 001", "001", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register 110", "110", info.getArg1());
		assertEquals("The value for second arg should be r2 010", "010", info.getArg2());
		assertEquals("This should not be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This should not be an memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 0000000 (default)", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter value should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testMOV_R4_R5_lower()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("mov");
		line.setFirstArg("r4");
		line.setSecondArg("r5");
		line.setThirdArg(null);

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value of alu op should be 11", "11", info.getAlu());
		assertEquals("The value for write register should be r4 100", "100", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register 110", "110", info.getArg1());
		assertEquals("The value for second arg should be r5 101", "101", info.getArg2());
		assertEquals("This should not be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This should not be an memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 0000000 (default)", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter value should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testMOVI_R1_0x32()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("movi");
		line.setFirstArg("r1");
		line.setSecondArg("00110010");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be r1 (001)", "001", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 00110010", "00110010", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testMOVI_R2_0x33()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("MOVI");
		line.setFirstArg("r2");
		line.setSecondArg("00110011");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be r2 (010)", "010", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 00110011", "00110011", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testNOP()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("nop");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be null register (110)", "110", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 00000000", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testJUMP()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("jump");
		line.setFirstArg("R3");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be null register (110)", "110", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be r3 (011)", "011", info.getArg2());
		assertEquals("This shouldn't be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 00000000", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter should be 01 (jump)", "01", info.getProgramCounter());
	}

	@Test
	public void testJUMPI()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("JUMPI");
		line.setFirstArg("01010101");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be null register (110)", "110", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 01010101", "01010101", info.getImmediateValue());
		assertEquals("The value for program counter should be 01 (jump)", "01", info.getProgramCounter());
	}

	@Test
	public void testJZ_R1_0x1F()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("JZ");
		line.setFirstArg("R5");
		line.setSecondArg("00001111");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be null register (110)", "110", info.getRegisterWrite());
		assertEquals("The value for first arg should be r5 (101)", "101", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 00001111", "00001111", info.getImmediateValue());
		assertEquals("The value for program counter should be 10 (jump if zero)", "10", info.getProgramCounter());
	}

	@Test
	public void testJNZ_R4_0xD0()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("JNZ");
		line.setFirstArg("R4");
		line.setSecondArg("11110000");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be null register (110)", "110", info.getRegisterWrite());
		assertEquals("The value for first arg should be r4 (100)", "100", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 11110000", "11110000", info.getImmediateValue());
		assertEquals("The value for program counter should be 11 (jump if zero)", "11", info.getProgramCounter());
	}

	@Test
	public void testADD_R2_R3_R4()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("Add");
		line.setFirstArg("R2");
		line.setSecondArg("r3");
		line.setThirdArg("R4");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 00 (sum)", "00", info.getAlu());
		assertEquals("The value for write register should be r2 (010)", "010", info.getRegisterWrite());
		assertEquals("The value for first arg should be r3 (011)", "011", info.getArg1());
		assertEquals("The value for second arg should be r4 (100)", "100", info.getArg2());
		assertEquals("This shouldn't be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be default 00000000", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testADDI_R2_R4_0x20()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("AdDi");
		line.setFirstArg("r2");
		line.setSecondArg("r4");
		line.setThirdArg("00100000");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 00 (sum)", "00", info.getAlu());
		assertEquals("The value for write register should be r2 (010)", "010", info.getRegisterWrite());
		assertEquals("The value for first arg should be r4 (100)", "100", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be default 00100000", "00100000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testAND_R3_R4_R5()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("and");
		line.setFirstArg("R3");
		line.setSecondArg("R4");
		line.setThirdArg("r5");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 01 (and)", "01", info.getAlu());
		assertEquals("The value for write register should be r3 (011)", "011", info.getRegisterWrite());
		assertEquals("The value for first arg should be r4 (100)", "100", info.getArg1());
		assertEquals("The value for second arg should be r5 (101)", "101", info.getArg2());
		assertEquals("This shouldn't be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be default 00000000", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testANDI_R3_R4_0x20()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("ANDI");
		line.setFirstArg("R3");
		line.setSecondArg("R4");
		line.setThirdArg("00100000");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 01 (and)", "01", info.getAlu());
		assertEquals("The value for write register should be r3 (011)", "011", info.getRegisterWrite());
		assertEquals("The value for first arg should be r4 (100)", "100", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This shouldn't be a memory read operation", "0", info.getMemoryRead());
		assertEquals("The value for immediate value should be 00100000", "00100000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testLOAD_R5_R0()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("LOAD");
		line.setFirstArg("r5");
		line.setSecondArg("r0");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be r5 (101)", "101", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be r0 (000)", "000", info.getArg2());
		assertEquals("This shouldn't be an immediate operation", "0", info.getImmediateOperation());
		assertEquals("This should be a memory read operation", "1", info.getMemoryRead());
		assertEquals("The value for immediate value should be default 00000000", "00000000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}

	@Test
	public void testLOADI_R5_0x80()
	{
		ProgramLine line = new ProgramLine();
		line.setCommand("LOADI");
		line.setFirstArg("R5");
		line.setSecondArg("01000000");

		AssemblerLineInfo info = MainClass.getAssemblerLineInfo(line);

		assertEquals("The value for alu should be 11 (immediate)", "11", info.getAlu());
		assertEquals("The value for write register should be r5 (101)", "101", info.getRegisterWrite());
		assertEquals("The value for first arg should be null register (110)", "110", info.getArg1());
		assertEquals("The value for second arg should be null register (110)", "110", info.getArg2());
		assertEquals("This should be an immediate operation", "1", info.getImmediateOperation());
		assertEquals("This should be a memory read operation", "1", info.getMemoryRead());
		assertEquals("The value for immediate value should be 01000000", "01000000", info.getImmediateValue());
		assertEquals("The value for program counter should be 00 (no jump)", "00", info.getProgramCounter());
	}
}
