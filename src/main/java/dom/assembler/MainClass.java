package dom.assembler;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static dom.assembler.AssemberCommands.Instructions.*;
import static dom.assembler.AssemberCommands.Registers.*;
import static dom.assembler.AssemberCommands.*;

public class MainClass
{
	public static void main(String args[])
	{
		boolean saveAsBinaryFile = false;

		if ( args.length == 0 )
		{
			System.out.println("Usage:");
			System.out.println("asembler {assembly program file name} [-b] [-o] [output file name]");
			return;
		}

		String inputFilename = args[0];
		String outputFilename = null;
		if ( args.length == 4 )
		{
			outputFilename = args[3];
		} else if ( args.length == 3 )
		{
			outputFilename = args[2];
		} else if ( args.length == 2 )
		{
			if ( "-b".equals(args[1]) )
			{
				saveAsBinaryFile = true;
			}
			outputFilename = inputFilename + ".bin";
		} else
		{
			outputFilename = inputFilename + ".bin";
		}


		List<String> programSource = loadProgram(inputFilename);
		List<ProgramLine> programLines = parseProgram(programSource);
		List<String> assembledProgramLines = assembleProgram(programLines);

		if ( saveAsBinaryFile )
		{
			// save as binary
		} else
		{
			saveAsTextFile(assembledProgramLines, outputFilename);
		}

	}

	private static List<String> loadProgram(String filename)
	{
		List<String> result = new ArrayList<>();
		try
		{
			FileReader fileReader = new FileReader(filename);
			BufferedReader reader = new BufferedReader(fileReader);
			String line;
			while ( (line = reader.readLine()) != null )
			{
				result.add(line);
			}
		} catch ( FileNotFoundException exc )
		{
			exc.printStackTrace();
		} catch ( IOException exc )
		{
			exc.printStackTrace();
		}
		return result;
	}

	protected static List<ProgramLine> parseProgram(List<String> programSource)
	{
		List<ProgramLine> result = new ArrayList<>();

		for ( int i = 0; i < programSource.size(); i++ )
		{
			ProgramLine line = new ProgramLine();

			String currentLine = programSource.get(i);
			if ( currentLine == null || "".equals(currentLine) )
				continue;
			if ( currentLine.contains(";") )
			{
				int index = currentLine.indexOf(";");
				currentLine = currentLine.substring(0, index);
			}
			String[] tokensArray = currentLine.split("\\s+");
			List<String> tokens = new ArrayList<>();


			for ( String s : tokensArray )
			{
				// remove ',' from tokens
				if ( s.endsWith(",") )
				{
					s = s.substring(0, s.length() - 1);
				}

				// change hex, bin and dev immediate values to binary
				if ( s.startsWith("0x") )
				{
					s = s.substring(2);
					int value = Integer.parseInt(s, 16);
					s = String.format("%8s", Integer.toBinaryString(value)).replace(' ', '0');
				} else if ( s.startsWith("0b") )
				{
					s = s.substring(2);
					int value = Integer.parseInt(s, 2);
					s = String.format("%8s", Integer.toBinaryString(value)).replace(' ', '0');
				} else if ( s.startsWith("0d"))
				{
					s = s.substring(2);
					int value = Integer.parseInt(s, 10);
					s = String.format("%s", Integer.toBinaryString(value).replace(' ', '0'));
				}

				tokens.add(s);
			}


			if ( tokens.size() == 4 )
			{
				line.setCommand(tokens.get(0));
				line.setFirstArg(tokens.get(1));
				line.setSecondArg(tokens.get(2));
				line.setThirdArg(tokens.get(3));
			} else if ( tokens.size() == 3 )
			{
				line.setCommand(tokens.get(0));
				line.setFirstArg(tokens.get(1));
				line.setSecondArg(tokens.get(2));
			} else if ( tokens.size() == 2 )
			{
				line.setCommand(tokens.get(0));
				line.setFirstArg(tokens.get(1));
			} else if ( tokens.size() == 1 )
			{
				line.setCommand(tokens.get(0));
			} else
			{
				System.err.println("error, there was a line with other length");
			}

			result.add(line);
		}

		return result;
	}

	private static List<String> assembleProgram(List<ProgramLine> programLines)
	{
		List<String> program = new ArrayList<>();

		for ( int i = 0; i < programLines.size(); i++ )
		{
			ProgramLine line = programLines.get(i);
			String assemblyLine = assembleSingleLine(line);
			program.add(assemblyLine);
		}
		return program;
	}

	protected static String assembleSingleLine(ProgramLine line)
	{
		AssemblerLineInfo lineInfo;
		lineInfo = getAssemblerLineInfo(line);
		String assemblyLine = "000000" + lineInfo.getProgramCounter() + "00" +
				lineInfo.getAlu() + "0" + lineInfo.getArg1() + lineInfo.getImmediateOperation() +
				lineInfo.getArg2() + lineInfo.getMemoryRead() + lineInfo.getRegisterWrite() +
				lineInfo.getImmediateValue();
		return assemblyLine;
	}

	protected static AssemblerLineInfo getAssemblerLineInfo(ProgramLine line)
	{
		String command = line.getCommand();
		if ( command != null )
			command = command.toLowerCase();
		String firstArg = line.getFirstArg();
		if ( firstArg != null )
			firstArg = firstArg.toLowerCase();
		String secondArg = line.getSecondArg();
		if ( secondArg != null )
			secondArg = secondArg.toLowerCase();
		String thirdArg = line.getThirdArg();
		if ( thirdArg != null )
			thirdArg = thirdArg.toLowerCase();

		AssemblerLineInfo result = new AssemblerLineInfo();

		result.setProgramCounter(getProgramCounter(command));
		result.setAlu(getAlu(command));
		result.setImmediateOperation(getImmediateOperation(command));
		result.setMemoryRead(getMemoryRead(command));
		switch ( command )
		{
			case MOV:
			{
				result.setArg1(NULL);
				result.setArg2(getRegister(secondArg));
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(DEFAULT_VALUE);
				break;
			}
			case MOVI:
			{
				result.setArg1(NULL);
				result.setArg2(NULL);
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(secondArg);
				break;
			}
			case NOP:
			{
				result.setArg1(NULL);
				result.setArg2(NULL);
				result.setRegisterWrite(NULL);
				result.setImmediateValue(DEFAULT_VALUE);
				break;
			}
			case JUMP:
			{
				result.setArg1(NULL);
				result.setArg2(getRegister(firstArg));
				result.setRegisterWrite(NULL);
				result.setImmediateValue(DEFAULT_VALUE);
				break;
			}
			case JUMPI:
			{
				result.setArg1(NULL);
				result.setArg2(NULL);
				result.setRegisterWrite(NULL);
				result.setImmediateValue(firstArg);
				break;
			}
			case JZ:
			{
				result.setArg1(getRegister(firstArg));
				result.setArg2(NULL);
				result.setRegisterWrite(NULL);
				result.setImmediateValue(secondArg);
				break;
			}
			case JNZ:
			{
				result.setArg1(getRegister(firstArg));
				result.setArg2(NULL);
				result.setRegisterWrite(NULL);
				result.setImmediateValue(secondArg);
				break;
			}
			case ADD:
			{
				result.setArg1(getRegister(secondArg));
				result.setArg2(getRegister(thirdArg));
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(DEFAULT_VALUE);
				break;
			}
			case ADDI:
			{
				result.setArg1(getRegister(secondArg));
				result.setArg2(NULL);
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(thirdArg);
				break;
			}
			case AND:
			{
				result.setArg1(getRegister(secondArg));
				result.setArg2(getRegister(thirdArg));
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(DEFAULT_VALUE);
				break;
			}
			case ANDI:
			{
				result.setArg1(getRegister(secondArg));
				result.setArg2(NULL);
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(thirdArg);
				break;
			}
			case LOAD:
			{
				result.setArg1(NULL);
				result.setArg2(getRegister(secondArg));
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(DEFAULT_VALUE);
				break;
			}
			case LOADI:
			{
				result.setArg1(NULL);
				result.setArg2(NULL);
				result.setRegisterWrite(getRegister(firstArg));
				result.setImmediateValue(secondArg);
				break;
			}
		}
		return result;
	}

	private static void saveAsTextFile(List<String> program, String filename)
	{
		PrintWriter writer = null;
		try
		{
			File outputFile = new File(filename);
			writer = new PrintWriter(outputFile);

			for ( int i = 0; i < program.size(); i++ )
				writer.println(program.get(i));
		} catch ( FileNotFoundException exc )
		{
			exc.printStackTrace();
		} finally
		{
			if ( writer != null )
				writer.close();
		}

		// save as text for verilog code
		try
		{
			File outputFile = new File(filename + ".vhdl_inst_list");
			writer = new PrintWriter(outputFile);

			for ( int i = 0; i < program.size(); i++ )
				writer.println("assign program[" + i + "]=32'b" + program.get(i) + ";");
			writer.println("genvar i;");
			writer.println("generate");
			writer.println("for ( i = " + program.size() + "; i < 256; i=i+1 )");
			writer.println("\tbegin");
			writer.println("\t\tassign program[i] = 32'b00000000001101101110011000000000;\t// nop");
			writer.println("\tend");
			writer.println("endgenerate;");
		} catch ( FileNotFoundException exc )
		{
			exc.printStackTrace();
		} finally
		{
			if ( writer != null )
				writer.close();
		}
	}
}
