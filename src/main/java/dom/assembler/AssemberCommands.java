package dom.assembler;

import java.util.HashMap;
import java.util.Map;
import static dom.assembler.AssemberCommands.Registers.*;
import static dom.assembler.AssemberCommands.Instructions.*;

public class AssemberCommands
{
	public class Instructions
	{
		public final static String MOV = "mov";
		public final static String MOVI = "movi";
		public final static String NOP = "nop";
		public final static String JUMP = "jump";
		public final static String JUMPI = "jumpi";
		public final static String JNZ = "jnz";
		public final static String JZ = "jz";
		public final static String ADD = "add";
		public final static String ADDI = "addi";
		public final static String AND = "and";
		public final static String ANDI = "andi";
		public final static String LOAD = "load";
		public final static String LOADI = "loadi";
	}

	public class Registers
	{
		public final static String R0 = "000";
		public final static String R1 = "001";
		public final static String R2 = "010";
		public final static String R3 = "011";
		public final static String R4 = "100";
		public final static String R5 = "101";
		public final static String R6 = "110";
		public final static String NULL = "110";
	}

	public final static String IMMEDIATE_OPERATION = "1";
	public final static String NO_IMMEDIATE_OPERATION = "0";

	public final static String MEMORY_READ = "1";
	public final static String NO_MEMORY_READ = "0";

	public final static String PC_NO_JUMP = "00";
	public final static String PC_JUMP = "01";
	public final static String PC_JUMP_IF_ZERO = "10";
	public final static String PC_JUMP_IF_NOT_ZERO = "11";

	public final static String ALU_ADD = "00";
	public final static String ALU_AND = "01";
	public final static String ALU_COMPARE = "01";
	public final static String ALU_IMMEDIATE = "11";




	public final static String DEFAULT_VALUE = "00000000";



	private final static Map<String, String> programCounterOperationValue = new HashMap<>();
	private final static Map<String, String> aluOperationValues = new HashMap<>();
	private final static Map<String, String> readOperationValues = new HashMap<>();
	private final static Map<String, String> immediateOperationValues = new HashMap<>();
	private final static Map<String, String> registers = new HashMap<>();

	public static String getProgramCounter(String instruction)
	{
		return programCounterOperationValue.get(instruction);
	}

	public static String getAlu(String instruction)
	{
		return aluOperationValues.get(instruction);
	}

	public static String getImmediateOperation(String instruction)
	{
		return immediateOperationValues.get(instruction);
	}

	public static String getMemoryRead(String instruction)
	{
		return readOperationValues.get(instruction);
	}

	public static String getRegister(String regName)
	{
		return registers.get(regName);
	}
	static
	{
		// initialize program counter values
		programCounterOperationValue.put(MOV, PC_NO_JUMP);
		programCounterOperationValue.put(MOVI, PC_NO_JUMP);
		programCounterOperationValue.put(NOP, PC_NO_JUMP);
		programCounterOperationValue.put(JUMP, PC_JUMP);
		programCounterOperationValue.put(JUMPI, PC_JUMP);
		programCounterOperationValue.put(JNZ, PC_JUMP_IF_NOT_ZERO);
		programCounterOperationValue.put(JZ, PC_JUMP_IF_ZERO);
		programCounterOperationValue.put(ADD, PC_NO_JUMP);
		programCounterOperationValue.put(ADDI, PC_NO_JUMP);
		programCounterOperationValue.put(AND, PC_NO_JUMP);
		programCounterOperationValue.put(ANDI, PC_NO_JUMP);
		programCounterOperationValue.put(LOAD, PC_NO_JUMP);
		programCounterOperationValue.put(LOADI, PC_NO_JUMP);

		// initialize alu operation values
		aluOperationValues.put(MOV, ALU_IMMEDIATE);
		aluOperationValues.put(MOVI, ALU_IMMEDIATE);
		aluOperationValues.put(NOP, ALU_IMMEDIATE);
		aluOperationValues.put(JUMP, ALU_IMMEDIATE);
		aluOperationValues.put(JUMPI, ALU_IMMEDIATE);
		aluOperationValues.put(JZ, ALU_IMMEDIATE);
		aluOperationValues.put(JNZ, ALU_IMMEDIATE);
		aluOperationValues.put(ADD, ALU_ADD);
		aluOperationValues.put(ADDI, ALU_ADD);
		aluOperationValues.put(AND, ALU_AND);
		aluOperationValues.put(ANDI, ALU_AND);
		aluOperationValues.put(LOAD, ALU_IMMEDIATE);
		aluOperationValues.put(LOADI, ALU_IMMEDIATE);



		// initialize immediate operation info
		immediateOperationValues.put(MOV, NO_IMMEDIATE_OPERATION);
		immediateOperationValues.put(MOVI, IMMEDIATE_OPERATION);
		immediateOperationValues.put(NOP, IMMEDIATE_OPERATION);
		immediateOperationValues.put(JUMP, NO_IMMEDIATE_OPERATION);
		immediateOperationValues.put(JUMPI, IMMEDIATE_OPERATION);
		immediateOperationValues.put(JZ, IMMEDIATE_OPERATION);
		immediateOperationValues.put(JNZ, IMMEDIATE_OPERATION);
		immediateOperationValues.put(ADD, NO_IMMEDIATE_OPERATION);
		immediateOperationValues.put(ADDI, IMMEDIATE_OPERATION);
		immediateOperationValues.put(AND, NO_IMMEDIATE_OPERATION);
		immediateOperationValues.put(ANDI, IMMEDIATE_OPERATION);
		immediateOperationValues.put(LOAD, NO_IMMEDIATE_OPERATION);
		immediateOperationValues.put(LOADI, IMMEDIATE_OPERATION);


		// initialize memory read info
		readOperationValues.put(MOV, NO_MEMORY_READ);
		readOperationValues.put(MOVI, NO_MEMORY_READ);
		readOperationValues.put(NOP, NO_MEMORY_READ);
		readOperationValues.put(JUMP, NO_MEMORY_READ);
		readOperationValues.put(JUMPI, NO_MEMORY_READ);
		readOperationValues.put(JZ, NO_MEMORY_READ);
		readOperationValues.put(JNZ, NO_MEMORY_READ);
		readOperationValues.put(ADD, NO_MEMORY_READ);
		readOperationValues.put(ADDI, NO_MEMORY_READ);
		readOperationValues.put(AND, NO_MEMORY_READ);
		readOperationValues.put(ANDI, NO_MEMORY_READ);
		readOperationValues.put(LOAD, MEMORY_READ);
		readOperationValues.put(LOADI, MEMORY_READ);

		registers.put("r0", R0);
		registers.put("r1", R1);
		registers.put("r2", R2);
		registers.put("r3", R3);
		registers.put("r4", R4);
		registers.put("r5", R5);
		registers.put("r6", R6);
	}
}
