package dom.assembler;

public class AssemblerLineInfo
{
	private String programCounter;
	private String alu;
	private String arg1;
	private String immediateOperation;
	private String arg2;
	private String memoryRead;
	private String registerWrite;
	private String immediateValue;

	public String getProgramCounter()
	{
		return programCounter;
	}

	public void setProgramCounter(String programCounter)
	{
		this.programCounter = programCounter;
	}

	public String getAlu()
	{
		return alu;
	}

	public void setAlu(String alu)
	{
		this.alu = alu;
	}

	public String getArg1()
	{
		return arg1;
	}

	public void setArg1(String arg1)
	{
		this.arg1 = arg1;
	}

	public String getArg2()
	{
		return arg2;
	}

	public void setArg2(String arg2)
	{
		this.arg2 = arg2;
	}

	public String getMemoryRead()
	{
		return memoryRead;
	}

	public void setMemoryRead(String memoryRead)
	{
		this.memoryRead = memoryRead;
	}

	public String getRegisterWrite()
	{
		return registerWrite;
	}

	public void setRegisterWrite(String registerWrite)
	{
		this.registerWrite = registerWrite;
	}

	public String getImmediateValue()
	{
		return immediateValue;
	}

	public void setImmediateValue(String immediateValue)
	{
		this.immediateValue = immediateValue;
	}

	public String getImmediateOperation()
	{
		return immediateOperation;
	}

	public void setImmediateOperation(String immediateOperation)
	{
		this.immediateOperation = immediateOperation;
	}
}
