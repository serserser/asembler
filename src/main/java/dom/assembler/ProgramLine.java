package dom.assembler;

public class ProgramLine
{
	String command;
	String firstArg;
	String secondArg;
	String thirdArg;

	public String getCommand()
	{
		return command;
	}

	public void setCommand(String command)
	{
		this.command = command;
	}

	public String getFirstArg()
	{
		return firstArg;
	}

	public void setFirstArg(String firstArg)
	{
		this.firstArg = firstArg;
	}

	public String getSecondArg()
	{
		return secondArg;
	}

	public void setSecondArg(String secondArg)
	{
		this.secondArg = secondArg;
	}

	public String getThirdArg()
	{
		return thirdArg;
	}

	public void setThirdArg(String thirdArg)
	{
		this.thirdArg = thirdArg;
	}
}
